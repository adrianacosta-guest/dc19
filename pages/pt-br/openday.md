---
name: Open Day
---
# Open Day (Dia Livre)

O Open Day é destinado ao público em geral. **Serão oferecidas
atividades em português e outros idiomas** para pessoas com interesses mais
amplos além do Debian, contendo desde tópicos específicos do Debian até assuntos
gerais relacionados a comunidade de Software Livre e Código Aberto, e o movimento
maker.

O evento é uma oportunidade perfeita para os(as) usuários(as) locais
interessados(as) conhecerem a nossa comunidade, para aumentarmos a comunidade
Debian, e para que os nossos patrocinadores aumentem sua visibilidade durante
a feira de empregos.

Com menos atividades puramente técnicas em comparação à programação principal da
DebConf, as atividades no Open Day cobrirão uma grande variedade de tópicos,
desde questões sociais e culturais até palestras e oficinas introdutórias ao
Debian.

Se você está interessado(a) em palestrar no Open Day sobre qualquer assunto
relacionado a Software Livre e Código Aberto, por favor leia a nossa
[chamada de atividades](/cfp/) e envie sua(s) proposta(s).

Se você tiver sugestões de palestrantes para convidarmos, por favor envie um
e-mail para <content@debconf.org>. Toda sugestão é bem-vinda.

A participação no Open Day é totalmente gratuita, assim como acontece em toda a
DebConf. Mesmo se você for participar apenas do Open Day, é muito importante que
você realize a sua [inscrição simplificada](/pt-br/inscricao/) em nosso sistema.

## Quando?

 * 20 de julho de 2019 (sábado)
 * 9:00h às 18:00h

## Onde?

No [Campus central da Universidade Tecnológica Federal do Paraná (UTFPR)](/about/venue/)
em Curitiba.

## Feira de empregos

Durante o Open Day teremos uma feira de empregos com estandes dos nossos
[patrocinadores](/sponsors/).

## FAQ

### Onde eu envio a minha proposta de atividade?

Abriremos a [chamada de atividades](/cfp/) no início de 2019. Lá você encontrará
todas as informações necessárias.

### As atividades serão em quais idiomas?

O Open Day terá atividades em vários idiomas. Esperamos ter atividades em
português, inglês e espanhol.

Se sua palestra será em português, você pode escrever o campo Abstract
em português também.

### Preciso de ajuda para instalar o Debian, e agora?

Teremos um *install fest* (festival de instalação) para ajudar aqueles(as)
participantes que gostariam de instalar o sistema operacional Debian
em seus computadores/notebooks. Leve o seu equipamento e receba ajuda
especializada da comunidade Debian!

## Como posso ajudar a organizar o Open Day?

A DebConf é realizada graças aos esforços de voluntários(as). Se você quiser
ajudar, veja nossa [página de contatos](/contact/). O Open Day está
sendo organizado na nossa [wiki](https://wiki.debian.org/DebConf/19/OpenDay/Planning).
