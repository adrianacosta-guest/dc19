---
name: Child care
---
# Child care

During DebConf19, there will be a room at UTFPR where parents who want
to attend the event with their children will be able to leave them
under the care of daycare professionals, for free.

This room will have two daycare professionals who will be responsible
for entertaining the children while their parents participate in
DebConf19's activities.

This service will be paid for by the organization of the event.
