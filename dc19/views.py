from bakery.views import BuildableTemplateView


class IndexView(BuildableTemplateView):
    template_name = 'wafer/index.html'
    build_path = 'index.html'
